# Golang demos #


Try this

```
#!bash

go get bitbucket.org/go_lang/ex/<package>
go test bitbucket.org/go_lang/ex/<package>
```

Example:
```
#!bash
# install Go, set GOPATH env to r/w dir
go get bitbucket.org/go_lang/ex/logger
cd $GOPATH/src/bitbucket.org/go_lang/ex/logger/
git tag -ln
git checkout logger.v1
go test bitbucket.org/go_lang/ex/logger
# this test takes about 1 second
# get back to the master
git checkout master
```

## Packages

* __logger__ - non trivial tests for go-routines

## Others

Please visit out community blog at http://vk.com/go_lang (in russian).
You can find lots of examples there.
All examples supplied with comments in english.