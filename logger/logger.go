package logger

type TestLogger interface {
	CallLog(string)
}

// Logger, adds messages to `msgs`.
type Logger struct {
	msgs    []string
	logchan chan string
	getchan chan *MsgsEnvelope
}

// Logger envelop.
// Carry n messages in msgs chan.
type MsgsEnvelope struct {
	n    int
	msgs chan []string
}

func NewLogger() *Logger {
	l := new(Logger)
	l.logchan = make(chan string, 64) // some buffer makes test unstable
	l.getchan = make(chan *MsgsEnvelope)

	go l.doLog()
	return l
}

func (l *Logger) Log(msg string) {
	l.logchan <- msg
}

func (l *Logger) GetLast(n int) []string {
	ret := make(chan []string)
	l.getchan <- &MsgsEnvelope{n, ret}
	return <-ret
}

func (l *Logger) doLog() {
	for {
		select {
		case m := <-l.logchan:
			l.msgs = append(l.msgs, m)

			if t, ok := interface{}(l).(TestLogger); ok {
				t.CallLog(m)
			}

		case g := <-l.getchan:
			if len(l.msgs) > g.n {
				g.msgs <- l.msgs[len(l.msgs)-g.n:]
			} else {
				g.msgs <- l.msgs
			}
		}
	}
}
