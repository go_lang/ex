package logger

import (
	"testing"
)

var cLog = make(chan string)

func (l *Logger) CallLog(m string) {
	cLog <- m
}

func TestLogMethod(t *testing.T) {

	var msgs = []string{
		"Dante",
		"travels",
		"through",
		"the",
		"centre",
		"of",
		"the",
		"Earth",
		"in",
		"the",
		"Inferno",
	}

	l := NewLogger()
	for _, msg := range msgs {
		l.Log(msg)
		// Call with:
		//   go test -v bitbucket.org/go_lang/ex/logger
		t.Log(<-cLog)
	}

	var nLast = 3
	last := l.GetLast(nLast)
	if len(last) != nLast {
		t.Errorf("len(last) == %d // want %d", len(last), nLast)
	}
	for k, msg := range last {
		if msg != msgs[len(msgs)-nLast+k] {
			t.Errorf(`msgs[%d] == "%s" // want "%s"`,
				k, msg, msgs[len(msgs)-nLast+k])
		}
	}
}
